/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Aplicacional;

import Dominio.FamiliaFibra;
import Dominio.Orcamento;
import Dominio.PedidoOrcamento;
import Dominio.TipoCor;
import Persistencia.FamiliasFibraRepositorio;
import Persistencia.TiposCorRepositorio;
import java.util.ArrayList;

/**
 *
 * @author Utilizador
 */
public class OrcamentoController {

    public FamiliasFibraRepositorio fR;
    public TiposCorRepositorio fTC;

    public OrcamentoController() {
        fR = new FamiliasFibraRepositorio();
        fTC = new TiposCorRepositorio();
    }

    public ArrayList getFamFib() {
        ArrayList<FamiliaFibra> lFib = new ArrayList<>();
        lFib = fR.getFamiliaFibras();

        return lFib;
    }

    public ArrayList getListCor() {
        ArrayList<TipoCor> lTCor = new ArrayList<>();
        lTCor = fTC.getListaCores();
        return lTCor;
    }

    public void calcularOrcamento(int fam, int cor, int qtd) {

        FamiliaFibra ff = fR.getFibra(fam);
        TipoCor tc = fTC.getTipoCor(cor);

        PedidoOrcamento p = new PedidoOrcamento(ff, tc, qtd);
        Orcamento or = new Orcamento(p);

    }

}
