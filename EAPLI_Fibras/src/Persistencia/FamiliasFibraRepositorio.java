/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Dominio.FamiliaFibra;
import java.util.ArrayList;

/**
 *
 * @author Utilizador
 */
public class FamiliasFibraRepositorio {

     private ArrayList<FamiliaFibra> lF;
    
    public FamiliasFibraRepositorio() {
        lF= new ArrayList<>();
    }
    
   
    
    public ArrayList getFamiliaFibras(){
        return lF;
    }
    
     public FamiliaFibra getFibra(int fam) {
         return lF.get(fam);
    }
    
}
