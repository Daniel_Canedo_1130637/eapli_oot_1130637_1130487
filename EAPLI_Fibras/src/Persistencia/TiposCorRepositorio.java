/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Dominio.TipoCor;
import java.util.ArrayList;

/**
 *
 * @author Utilizador
 */
public class TiposCorRepositorio {

    private ArrayList<TipoCor> lC;

    public TiposCorRepositorio() {
        lC=new ArrayList<>();
    }

    public ArrayList getListaCores() {
        return lC;
    }
    
      public TipoCor getTipoCor(int cor) {
        return lC.get(cor);
    }
}
